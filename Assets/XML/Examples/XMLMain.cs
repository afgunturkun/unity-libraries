﻿using System.Collections;
using System.Collections.Generic;
using System.IO;
using UnityEngine;

public class XMLMain : MonoBehaviour {

    public WeaponContainer weaponContainer;

    // Use this for initialization
    void Start () {
        weaponContainer = new WeaponContainer();

        weaponContainer.Weapons.Add(new Weapon() { Name = "Weapon 1"});

        weaponContainer.Save(Path.Combine(Application.dataPath, "weapons.xml"));

        var monsterCollection = WeaponContainer.Load(Path.Combine(Application.dataPath, "weapons.xml"));
    }
	
	// Update is called once per frame
	void Update () {
		
	}
}
