﻿using System.Xml;
using System.Xml.Serialization;

public class Weapon
{
    [XmlAttribute("name")]
    public string Name;

    [XmlAttribute("attack power")]
    public float attackPower;

    [XmlAttribute("max ammo")]
    public float maxAmmo;

    [XmlAttribute("reload time")]
    public float reloadTime;
}